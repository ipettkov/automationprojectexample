package shopPragmaticSearchTests.positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class SearchAndAddToCartTest {

    WebDriver driver;
    Select dropDown;
    Actions action;
    WebDriverWait wait;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg/index.php?route=common/home");
    }

    @AfterMethod
    public void killBrowser() {
        driver.quit();
    }

    @Test
    public void searchTest() {
        wait = new WebDriverWait(driver, 10);
        action = new Actions(driver);


        action.sendKeys(driver.findElement(By.id("search")), "nekyvtekst")
                .click(driver.findElement(By.cssSelector("div#search i[class*='fa fa-search']"))).perform();
        wait.until(webDriver -> webDriver.findElement(By.cssSelector("div#content > h1")).getText().equalsIgnoreCase("Search - nekyvtekst"));

        assertEquals(driver.findElement(By.cssSelector("div#content > h1")).getText(), "Search - nekyvtekst");
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div#search > input[name='search']")));
        driver.findElement(By.cssSelector("div#search > input[name='search']")).clear();

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div#search > input[name='search']")));

        action.sendKeys(driver.findElement(By.id("search")), "iPhone")
                .click(driver.findElement(By.xpath("//div[@id='search']//following-sibling::button[@class='btn btn-default btn-lg']"))).perform();

        dropDown = new Select(driver.findElement(By.cssSelector("div#content select[name='category_id']")));

        wait.until(webDriver -> webDriver.findElement(By.cssSelector("div#content select[name='category_id']")));

        assertFalse(dropDown.isMultiple());

        dropDown.selectByValue("24");

        WebElement subCatCheckbox = driver.findElement(By.cssSelector("label[class='checkbox-inline'] > input[name='sub_category']"));
        WebElement descriptionsCheckBox = driver.findElement(By.id("description"));

        if (!subCatCheckbox.isSelected()) {
            subCatCheckbox.click();
        }

        if (!descriptionsCheckBox.isSelected()) {
            descriptionsCheckBox.click();
        }


    }

}
